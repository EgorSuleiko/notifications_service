from sqlalchemy import create_engine, update
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from config import DATABASE_NAME, USER_NAME, USER_PASSWORD, HOST
import tables


class DataBase:

    def __init__(self, database_name=DATABASE_NAME, user_name=USER_NAME, user_password=USER_PASSWORD, host=HOST):
        self.engine = create_engine(f'postgresql+psycopg2://{user_name}:{user_password}@{host}/{database_name}')
        self.engine.connect()


    def create_new_database(self, database_name=DATABASE_NAME, user_name=USER_NAME, user_password=USER_PASSWORD):
        connection = psycopg2.connect(database="postgres", user=user_name, password=user_password)
        connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

        cursor = self.connection.cursor()
        cursor.execute(f'create database {database_name}')
        cursor.close()
        connection.close()


class Client:

    def __init__(self, phone_num, code_num, tag, timezone):
        self.phone_num = phone_num
        self.code_num = code_num
        self.tag = tag
        self.timezone = timezone

    def insert_in_db(self, database):
        insertion = tables.clients.insert().values(
            phone_num=self.phone_num,
            code_num=self.code_num,
            tag=self.tag,
            timezone=self.timezone
        )
        connection = database.engine.connect()
        execution = connection.execute(insertion)
        self.id = execution.inserted_primary_key[0]

    @staticmethod
    def update_in_db(database, id_num, new_phone_num, new_code_num, new_tag, new_timezone):
        updation = tables.clients.update().where(
            tables.clients.c.id == id_num
        ).values(
            phone_num=new_phone_num,
            code_num=new_code_num,
            tag=new_tag,
            timezone=new_timezone
        )
        connection = database.engine.connect()
        connection.execute(updation)

    @staticmethod
    def delete_in_db(database, id_num):
        deletion = tables.clients.delete().where(
            tables.clients.c.id == id_num
        )
        connection = database.engine.connect()
        connection.execute(deletion)


class Mailing:

    def __init__(self, start_time, message, filter, end_time):
        self.start_time = start_time
        self.message = message
        self.filter = filter
        self.end_time = end_time

    def insert_in_db(self, database):
        insertion = tables.mailings.insert().values(
            start_time=self.start_time,
            message=self.message,
            filter=self.filter,
            end_time=self.end_time
        )
        connection = database.engine.connect()
        connection.execute(insertion)


class Message:

    def __init__(self, start_time, mailing_id, client_id, delivered=False):
        self.start_time = start_time
        self.delivered = delivered
        self.mailing_id = mailing_id
        self.client_id = client_id

    def insert_in_db(self, database):
        insertion = tables.messages.insert().values(
            start_time=self.start_time,
            delivered=self.delivered,
            mailing_id=self.mailing_id,
            client_id=self.client_id
        )
        connection = database.engine.connect()
        connection.execute(insertion)
