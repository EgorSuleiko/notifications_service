from sqlalchemy import MetaData, Table, Integer, String, Column, DateTime, Boolean, ForeignKey

metadata = MetaData()

mailings = Table('mailings', metadata,
                 Column('id', Integer(), primary_key=True),
                 Column('start_time', DateTime(), ),
                 Column('message', String()),
                 Column('filter', String()),
                 Column('end_time', DateTime())
                 )

clients = Table('clients', metadata,
                Column('id', Integer(), primary_key=True),
                Column('phone_num', String()),
                Column('code_num', Integer()),
                Column('tag', String()),
                Column('timezone', String())
                )

messages = Table('messages', metadata,
                 Column('id', Integer(), primary_key=True),
                 Column('start_time', DateTime()),
                 Column('delivered', Boolean(), default=False),
                 Column('mailing_id', Integer(), ForeignKey('mailings.id')),
                 Column('client_id', Integer(), ForeignKey('clients.id'))
                 )
